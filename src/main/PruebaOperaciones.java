package main;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class PruebaOperaciones {
	
	private Operaciones op = null;
	private int a = 0;
	private int b = 0;
	private int expectedRes = 0;
	
	public PruebaOperaciones(int nuA, int nuB, int expected) {
        this.a = nuA;
        this.b = nuB;
        this.expectedRes = expected;
    }
	
	@Parameters(name = "prueba --> {0} + {1} = {2}")
	public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                 { 0, 0, 0 }, 
                 { 1, 1, 2 }, 
                 { 2, 1, 3 }, 
                 { 3, 2, 5 }, 
                 { 4, 3, 7 }, 
                 { 5, 5, 10 }, 
                 { 6, 8, 14 },
                 { 5, -7, -2},
                 { 1, 4, 3 },
                 { -1, 2, 5 },
                 { 20, 10, 31 },
                 { 15, 1, 15 },
           });
    }
	
	@Before
	public void init() {
		this.op = new Operaciones();		
	}
	
	@Test
	public void sumTest() {		
		assertEquals(this.expectedRes, this.op.suma(this.a, this.b));
	}

}
